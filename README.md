[![Powered By Vaadin on Kotlin](http://vaadinonkotlin.eu/iconography/vok_badge.svg)](http://vaadinonkotlin.eu)
[![pipeline status](https://gitlab.com/mvysny/vok-dataloader/badges/master/pipeline.svg)](https://gitlab.com/mvysny/vok-dataloader/commits/master)
[![Maven Central](https://maven-badges.herokuapp.com/maven-central/com.github.mvysny.vokdataloader/vok-dataloader/badge.svg)](https://maven-badges.herokuapp.com/maven-central/com.github.mvysny.vokdataloader/vok-dataloader)

# VOK DataLoaders

A simple API which unifies paged/sorted/filtered data fetching from backend systems.
The API targets the following use-cases:

* Fetched data can be shown in a table UI component which fetches pages of data, for example the [Vaadin Grid](https://vaadin.com/components/vaadin-grid).
The user can then sort/filter data, e.g. simply by clicking the table column header.

This library defines just the API. For concrete implementation please see the following projects:

Implementors (libraries that provide various implementations of the `DataLoader` interface):

* [vok-orm](https://github.com/mvysny/vok-orm) for a `DataLoader` loading
  entities from a SQL database;
* [vok-rest-client](https://github.com/mvysny/vaadin-on-kotlin/tree/master/vok-rest-client)
  for a `DataLoader` loading entities from a REST server.
* [UMN](https://gitlab.com/mvysny/umn) for a tiny NoSQL database with synchronization capabilities;
  it allows for fetching index keys via a `DataLoader`.

Consumers (frameworks that are able to consume `DataLoader` and display it):

* [vok-util-vaadin8](https://github.com/mvysny/vaadin-on-kotlin/tree/master/vok-util-vaadin8) which can wrap
`DataLoader` as Vaadin 8 `DataProvider` which allows you to set `DataLoader` to `Grid`s, `ComboBox`es etc.
* [vok-util-vaadin10](https://github.com/mvysny/vaadin-on-kotlin/tree/master/vok-util-vaadin10) which can wrap
`DataLoader` as Vaadin 10+ `DataProvider` which allows you to set `DataLoader` to `Grid`s, `ComboBox`es etc.
* [vok-rest](https://github.com/mvysny/vaadin-on-kotlin/tree/master/vok-rest) takes any `DataLoader` and uses
`DataLoaderCrudHandler` and [Javalin](https://javalin.io/) to expose beans over REST, with full support for
paging, filtering and sorting.

## About the `DataLoader`

The [DataLoader](src/main/kotlin/com/github/mvysny/vokdataloader/DataLoader.kt) is but a very simple interface:

```kotlin
interface DataLoader<T: Any> : Serializable {
    fun getCount(filter: Filter<T>?): Long
    fun fetch(filter: Filter<T>?, sortBy: List<SortClause>, range: LongRange): List<T>
}
```

It accesses a pageable/filtrable/sortable native data set of some sort. For example, a SQL `DataLoader` will run SELECT, take
the JDBC [ResultSet](https://docs.oracle.com/en/java/javase/11/docs/api/java.sql/java/sql/ResultSet.html) and convert every row into a Java Bean. Another example would be a REST endpoint `DataLoader` which
takes a list of JSON maps and converts every row (=JSON map) into a Java Bean.

The native data set is expected to contain zero, one or more native data rows. The `DataLoader` retrieves native data rows
and turns them into instances of bean of some type (one data row into one bean).

## Examples

### Feeding NoSQL Database to Android ListView/RecyclerView

You can take a `DataLoader` which takes items from a database, then wrap it in a `PageFetchingList` which
converts the `DataLoader` into the plain `List` but fetches and caches pages of data. Then you can easily
feed the list into Android's `ListAdapter`, to display the data. This approach will load the data lazily as
the user scrolls, hence it's very memory-efficient:

```kotlin
val index = umn.getIndex(surnames)
val dataLoader = index.keyLoader()
val list = dataLoader.asList(30) // wraps with PageFetchingList
val adapter = ArrayAdapter(ctx, android.R.layout.simple_list_item_1, Collections.unmodifiableList(list))
```

### Feeding SQL Database Entities Into Vaadin `Grid`

Another example: you can use Vaadin-on-Kotlin-provided adapters to adapt `DataLoader` into Vaadin's `DataProvider` which
can then be fed directly to Vaadin Grid. For example, for Person entities loaded from a SQL database
via vok-orm:

```kotlin
val dataLoader = Person.dataLoader
val dp = DataLoaderAdapter(Person::class.java, dataLoader, { it.id!! }).withConfigurableFilter2()
grid.dataProvider = dp
```

## `DataLoader` Properties

Both [filters](src/main/kotlin/com/github/mvysny/vokdataloader/Filters.kt) and sort clauses accept the property names upon which the sorting
or filtering should occur. However, there is a very important distinction to make,
between _native_ properties and _dataloader_ properties.

### Native Properties

Native properties are named according to the naming of the native raw data row as loaded by the `DataLoader`.

For example:

* A SQL `DataLoader` loads from an outcome of a SQL SELECT, via JDBC's `ResultSet`. The data row
  in this case is a single row in the SQL SELECT which is a collection of database columns. The native property names are
  for example a database table column name (`PERSON_NAME`), or a column reference (`p.person_name` in
  `select p.person_name from Person p`).
* For a REST `DataLoader` this follows the REST endpoint naming scheme of the JSON maps returned via the GET.
  For example if the REST returns `[{"person_name": "John"}]`,
  then the row contains the property of `person_name`.
* For an in-memory collection of Java Beans, the native property name is already the Java Bean Property name, and
  therefore there is no distinction to `DataLoaderPropertyName` in this case.

### [DataLoaderPropertyName](src/main/kotlin/com/github/mvysny/vokdataloader/DataLoaderPropertyName.kt)

A name of a single property that the `DataLoader` accepts as a filter property name, or a sort clause property name.
Since every `DataLoader` produces Java Beans, every `DataLoader` **MUST** support Java Bean Property names
as `DataLoaderPropertyName`. In addition to that, the `DataLoader` **MAY** decide to
additionally accept `NativePropertyName`s as `DataLoaderPropertyName`s, to allow the user to reference native properties in filters and
sort clauses which aren't mapped to the Java Bean.
Every `DataLoader` **MUST** document what exactly he accepts (and what he doesn't accept).

For example:

* [vok-orm](https://github.com/mvysny/vok-orm) maps SQL SELECT outcome to Java/Kotlin class. It needs to map
`DataLoaderPropertyName` to `NativePropertyName` manually (can't use SQL aliases to map `DataLoaderPropertyName` to `NativePropertyName`
e.g. `select p.person_name as personName`) since SQL databases can not create WHERE
clauses based on aliases: [vok-orm issue 5](https://github.com/mvysny/vok-orm/issues/5). `vok-orm` is therefore
using the `@As` annotation on the Java Bean Property. In addition, however, we must support sorting and filtering based on
`NativePropertyName` to allow filtering on columns not returned/mapped to Java Bean; in this case the native property name
is for example a database table column name (`PERSON_NAME`), or a column reference (`p.person_name` in
  `select p.person_name from Person p`).
* [vok-rest-client](https://github.com/mvysny/vaadin-on-kotlin/tree/master/vok-rest-client) uses [Gson](https://github.com/google/gson) to turn list of
JSON maps into a list of Java Beans. It is a good practice for the REST filter names and the sorting criteria property naming
to follow names of keys in the JSON maps, and hence we will most probably have a Java Bean Property for every filter
or sort clause we can have. REST endpoints may decide to use the `lowercase_underscore` (or any other) naming scheme;
it is therefore the responsibility of REST `DataLoader` to e.g. to configure Gson to use column name aliases, for example using the `@As` annotation.

## [Filters](src/main/kotlin/com/github/mvysny/vokdataloader/Filters.kt)

The `DataLoader` API provides pre-defined set of basic filters frequently used in data storages.
All filters implement the [`Filter`](src/main/kotlin/com/github/mvysny/vokdataloader/Filters.kt#19) interface - feel free to go and create your own
filter implementations for your custom `DataLoader`.

There are the following filters available:

* `EqFilter` passes only rows with property of given value.
* You use `OpFilter` to use compare operators such as lt/less-than, le/less-than-or-equal,
  eq/equal, gt/greater-than, ge/greater-than-or-equal
* `InFilter` to pass rows with property of one of given values.
* `IsNullFilter` to pass rows with property value being null,
* `IsNotNullFilter` to pass rows with property value being not null,
* `StartsWithFilter` performing case-senstive/case-insensitive 'starts-with' (`foo LIKE 'bar%'`/`foo ILIKE 'bar%'` in SQL)
* `NativeSqlFilter` allows you to write a native SQL WHERE clause. Doesn't work with `ListDataLoader`
  since it doesn't support in-memory filtering.
* `SubstringFilter` performs the case-sensitive/case-insensitive 'substring' matching.
  **WARNING**: poor performance on SQL databases; it's much better to use `FullTextFilter`.
  Usually only used for in-memory filtering. vok-orm produces this kind of SQLs: `foo LIKE '%bar%'`/`foo ILIKE '%bar%'`.
* `FullTextFilter` performs the case-insensitive full-text search: any probe text
   must either contain all words in this query, or the query words must match beginnings
   of all of the words contained in the probe string.
   Different implementors will implement this differently and may have different
   requirements on how to configure the storage for full-text search; see below for SQL tips.

Additionally, you can wrap filters in the following combiners:

* `AndFilter` only passes rows which comply with all of given filters,
* `OrFilter` only passes rows which comply with any of given filters,
* `NotFilter` only passes rows which do not pass the underlying filter.

### In-memory filtering

The `Filter.test()` function gives an opportunity to every filter to also perform filtering
in-memory. You can use [`ListDataLoader`](src/main/kotlin/com/github/mvysny/vokdataloader/ListDataLoader.kt) to wrap a list of items in a `DataLoader` API;
alternatively you can use `dataLoader` extension property to wrap the list for you:

```kotlin
val provider: DataLoader<String> = listOf("a", "b", "c").dataLoader
```

If you only use filters with an in-memory `DataLoader`, you can take advantage
of property paths:

```kotlin
data class Person(var address: Address)
data class Address(var city: String)

val citizens: DataLoader<Person> = getInMemoryListOfEarthInhabitants() // might throw OutOfMemoryError ;)
val helsinkiCitizens = citizens.withFilter(EqFilter<Person>("address.city", "Helsinki"))
```

Certain filters may not be able to perform in-memory filtering; in such case
it's good to document this fact in the filter's [KDoc](https://kotlinlang.org/docs/reference/kotlin-doc.html) and make the `test()` function
throw an `Exception`. See `NativeSqlFilter` sources for an example.

### Applying Filters

Sometimes you need to always apply given filter to a `DataLoader`. There are
two ways to do this.

The first approach is when you're referencing Person properties directly, and
your code creates the filter directly:

```kotlin
val citizens: DataLoader<Person> = getInMemoryListOfEarthInhabitants() // might throw OutOfMemoryError ;)
val teens: DataLoader<Person> = citizens.withFilter { (Person::age ge 10) and (Person::age le 19) }
```

This approach is called the filter builder pattern and is implemented in the `FilterBuilder` class.
This approach works best for static filters.

> Please note that `withFilter` will create a new DataProvider with given filter always
> applied. The current DataLoader remains unaffected by the change and will not apply any filters.

However if you need to construct dynamic filters
(e.g. from components placed inside of the filter bar), then it's better to construct the
instances of filter classes:

```kotlin
val citizens: DataLoader<Person> = getInMemoryListOfEarthInhabitants() // might throw OutOfMemoryError ;)
val helsinkiCitizens = citizens.withFilter(EqFilter<Person>("address.city", "Helsinki"))
```

### Providing Default Sorting

If you'd like the DataLoader to always sort by given property, you can take advantage
of the `DataLoader.sortedBy()` extension method:

```kotlin
val citizens: DataLoader<Person> = getEarthInhabitants()
    .sortedBy(Person::surname.asc)
```

or

```kotlin
val citizens: DataLoader<Person> = getEarthInhabitants()
    .sortedBy("address.city".asc)
```

> Please note that `sortedBy` will create a new DataProvider with given filter always
> applied. The current DataLoader remains unaffected by the change and will not apply any filters.

### SQL Tips

See [vok-orm](https://github.com/mvysny/vok-orm) for two `DataLoader` implementations which you use
to access your SQL database easily.

It's very important to create proper database indices for any database column being filtered.
If the index is missing, the database may be forced to perform a full table scan,
causing significant performance drop.

Using `FullTextFilter` without creating a full-text index in the database will either perform
very poorly, or will not work at all (the database will simply return 0 results).
For example to have a full-text search in an SQL database/RDBMS system please use the
[vok-orm](https://github.com/mvysny/vok-orm) library and read the
[Full-Text Filters chapter](https://github.com/mvysny/vok-orm#full-text-filters).

### REST Tips

Often REST endpoints limit the number of items they can return, to 100 items (or lower).
Such `DataLoader` would then fail if there is a request to fetch 200 items for example.

In such case you can use [`ChunkFetchingLoader`](src/main/kotlin/com/github/mvysny/vokdataloader/ChunkFetchingLoader.kt) which wraps given REST `DataLoader`
and makes repeated calls to the underlying `DataLoader`, to honor the item fetch limit.

See [vok-rest-client](https://github.com/mvysny/vaadin-on-kotlin/tree/master/vok-rest-client)
for a REST client `DataLoader` implementation.

## Implementing `DataLoader` Tutorial

Please find more documentation in [Accessing NoSQL or REST data sources](http://www.vaadinonkotlin.eu/nosql_rest_datasources.html).

## Android Compatibility

vok-dataloader `0.9.x` (0.9.1) is the last version targeting ancient Android versions: it's tested on Java 7 JDK and compiled against older Kotlin.
vok-dataloader `0.10` will require Java 8 JDK or newer; that's still quite old and should work well on Androids 6+ (API level 23+).

