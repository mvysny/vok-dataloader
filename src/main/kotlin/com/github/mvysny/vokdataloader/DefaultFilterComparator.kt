package com.github.mvysny.vokdataloader

/**
 * Handles additional compare cases:
 * 1. Comparing of [Number]s of different types
 * 2. Comparing of [String]s to anything else.
 *
 * If you need to modify the functionality of this comparator, chain this comparator
 * after your custom comparator, then set the resulting comparator to the
 * [CompareOperator.comparator] field.
 */
public object DefaultFilterComparator : Comparator<Comparable<Any>> {
    override fun compare(o1: Comparable<Any>, o2: Comparable<Any>): Int {
        if (o1.javaClass == o2.javaClass) {
            return o1.compareTo(o2)
        }
        if (o1 is Number && o2 is Number) {
            return o1.toBigDecimal().compareTo(o2.toBigDecimal())
        }
        if (o1.javaClass == String::class.java || o2.javaClass == String::class.java) {
            return o1.toString().compareTo(o2.toString())
        }
        // fall back to the default implementation which will probably just fail
        return o1.compareTo(o2)
    }
}