package com.github.mvysny.vokdataloader

/**
 * Overcomes a natural limit of the number of items which [delegate] can fetch (via [DataLoader.fetch]). If more items are requested,
 * this data loader polls the delegate in chunks and reconstructs the result. Ideal for REST endpoints which are often
 * limited in how many items they can fetch.
 * @property delegate the data loader which imposes limit on the number of data fetched
 * @property delegateFetchLimit the known limit of items which the delegate can fetch, 1 or more.
 */
public class ChunkFetchingLoader<T: Any>(
        public val delegate: DataLoader<T>,
        public val delegateFetchLimit: Long
) : DataLoader<T> {
    init {
        require(delegateFetchLimit >= 1) { "delegateFetchLimit should be 1 or more but was $delegateFetchLimit" }
    }
    override fun toString(): String = "ChunkFetchingLoader(delegateFetchLimit=$delegateFetchLimit, delegate=$delegate)"
    override fun fetch(filter: Filter<T>?, sortBy: List<SortClause>, range: LongRange): List<T> {
        if (range.isEmpty()) return listOf()
        require(range.start >= 0) { "range $range contains negative indices" }
        val result: MutableList<T> = mutableListOf<T>()
        while(result.size < range.length) {
            val itemsToFetch: Long = (range.length - result.size).coerceAtMost(delegateFetchLimit)
            val offset: Long = range.start + result.size
            val fetchRange: LongRange = offset until (offset + itemsToFetch)
            val fetched: List<T> = delegate.fetch(filter, sortBy, fetchRange)
            result.addAll(fetched)
            if (fetched.size < itemsToFetch) {
                // we probably hit an end of this data loader, bail out
                break
            }
        }
        return result
    }
    override fun getCount(filter: Filter<T>?): Long = delegate.getCount(filter)
}

/**
 * Overcomes a natural limit of the number of items which [this] can fetch (via [DataLoader.fetch]). If more items are requested,
 * the returned data loader polls [this] in chunks and reconstructs the result. Ideal for REST endpoints which are often
 * limited in how many items they can fetch.
 * @param delegateFetchLimit the known limit of items which the delegate can fetch, 1 or more.
 */
public fun <T: Any> DataLoader<T>.limitChunkSize(delegateFetchLimit: Long): DataLoader<T> =
        ChunkFetchingLoader(this, delegateFetchLimit)
