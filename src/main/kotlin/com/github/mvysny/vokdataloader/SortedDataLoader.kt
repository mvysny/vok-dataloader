package com.github.mvysny.vokdataloader

/**
 * Returns a new data loader which appends given [sortBy] to any sorting criteria provided to the [DataLoader.fetch].
 * This means that anything provided to [DataLoader.fetch] takes precedence over [sortBy].
 */
public fun <T : Any> DataLoader<T>.sortedBy(vararg sortBy: SortClause): DataLoader<T> =
        SortedDataLoader(sortBy.toList(), this)

/**
 * Wraps [delegate] data loader and always appends given [sortBy] to any sorting criteria provided to the [DataLoader.fetch].
 * This means that anything provided to [DataLoader.fetch] takes precedence over [sortBy].
 *
 * You can use [DataLoader.sortedBy] convenience method to produce instances of this class.
 */
public class SortedDataLoader<T : Any>(
        public val sortBy: List<SortClause>,
        public val delegate: DataLoader<T>
) : DataLoader<T> {
    override fun getCount(filter: Filter<T>?): Long = delegate.getCount(filter)

    override fun fetch(filter: Filter<T>?, sortBy: List<SortClause>, range: LongRange): List<T> =
            delegate.fetch(filter, sortBy + this.sortBy, range)

    override fun toString(): String = "SortedDataLoader(sortBy=$sortBy, $delegate)"
}
