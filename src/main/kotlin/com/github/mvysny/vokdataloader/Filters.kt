package com.github.mvysny.vokdataloader

import java.io.Serializable

/**
 * A generic filter which filters items of type [T]. Implementors must precisely declare in kdoc how exactly the items are filtered.
 *
 * Implementor detail: [Any.equals]/[Any.hashCode]/[Any.toString] must be implemented properly, so that the filters can be
 * placed in a set. As a bare minimum, the filter type, the property name and the value which we compare against must be
 * taken into consideration.
 *
 * The [test] method is provided only as a convenience. There may be filters which
 * can not implement this kind of method
 * (e.g. REST-server-specific filters, or filters representing a SQL where clause).
 * Such filters should document this
 * behavior and may throw [UnsupportedOperationException] in the [test] method.
 * @param T the bean type upon which we will perform the filtering.
 */
public interface Filter<T : Any> : Serializable {
    /**
     * Evaluates this predicate on [t]. Returns `true` if the input argument
     * matches the predicate, otherwise `false`
     */
    public fun test(t: T): Boolean

    public infix fun and(other: Filter<in T>): Filter<T> = AndFilter(setOf<Filter<in T>>(this, other))
    public infix fun or(other: Filter<in T>): Filter<T> = OrFilter(setOf<Filter<in T>>(this, other))

    /**
     * Returns a filter that represents the logical negation of this filter.
     */
    public operator fun not(): Filter<T> = NotFilter(this)
}

/**
 * Filters beans by comparing a property value with given [propertyName]
 * to some expected [value]. Check out implementors for further details.
 *
 * The filter's [test] method is able to retrieve values of [propertyName]
 * containing a [PropertyPath]. This is only useful for in-memory filtering:
 * other types of filters generally may not support [PropertyPath]s or may interpret
 * them differently. For example a SQL Data Provider will interpret `e.name`
 * as the `name` column of table `EMPLOYEE` aliased as `e` in the select statement,
 * while [test] method will be unable to find the `e` property and will fail.
 *
 * @property propertyName a valid data loader property name.
 * See [DataLoaderPropertyName] for a precise definition.
 * @property value optional value to compare against.
 */
public abstract class BeanFilter<T : Any> : Filter<T> {
    public abstract val propertyName: DataLoaderPropertyName
    public abstract val value: Any?

    @Transient
    private var getter: PropertyPath<T>? = null

    /**
     * Gets the value of the [propertyName] for given [bean].
     * Very quick even though it uses reflection under the hood.
     *
     * Also able to retrieve a value of a property path, see [PropertyPath] for more details.
     */
    protected fun getValue(bean: T): Any? {
        var g: PropertyPath<T>? = getter
        if (g == null) {
            g = PropertyPath.of(bean.javaClass, propertyName)
            getter = g
        }
        return g.invoke(bean)
    }

    protected val formattedValue: String? get() = if (value != null) "'$value'" else null
}

/**
 * A filter which tests for value equality. Allows nulls.
 */
public data class EqFilter<T : Any>(override val propertyName: DataLoaderPropertyName, override val value: Any?) : BeanFilter<T>() {
    override fun toString(): String = "$propertyName = $formattedValue"
    override fun test(t: T): Boolean = getValue(t) == value
}

public enum class CompareOperator(public val sql92Operator: String) {
    eq("=") {
        override fun test(t: Comparable<Any>?, u: Comparable<Any>?): Boolean = comparator.compare(t, u) == 0
    },
    lt("<") {
        override fun test(t: Comparable<Any>?, u: Comparable<Any>?): Boolean = comparator.compare(t, u) < 0
    },
    le("<=") {
        override fun test(t: Comparable<Any>?, u: Comparable<Any>?): Boolean = comparator.compare(t, u) <= 0
    },
    gt(">") {
        override fun test(t: Comparable<Any>?, u: Comparable<Any>?): Boolean = comparator.compare(t, u) > 0
    },
    ge(">=") {
        override fun test(t: Comparable<Any>?, u: Comparable<Any>?): Boolean = comparator.compare(t, u) >= 0
    },
    ne("<>") {
        override fun test(t: Comparable<Any>?, u: Comparable<Any>?): Boolean = comparator.compare(t, u) != 0
    };

    /**
     * Compares two numbers (or comparable objects in general), returning true if
     * the values comply with the compare operator.
     */
    public abstract fun test(t: Comparable<Any>?, u: Comparable<Any>?): Boolean

    public companion object {
        /**
         * The default comparator used to compare values. Defaults to [DefaultFilterComparator]`.nullsFirst()`;
         * set to something else to support more comparisons (e.g. date-based comparisons).
         */
        public var comparator: Comparator<Comparable<Any>> = DefaultFilterComparator.nullsFirst()
    }
}

/**
 * A filter which supports less than, less or equals than, etc. Filters out null values.
 */
public data class OpFilter<T : Any>(override val propertyName: DataLoaderPropertyName, override val value: Comparable<*>, val operator: CompareOperator) : BeanFilter<T>() {
    override fun toString(): String = "$propertyName ${operator.sql92Operator} $formattedValue"

    @Suppress("UNCHECKED_CAST")
    override fun test(t: T): Boolean = operator.test(getValue(t) as Comparable<Any>?, value as Comparable<Any>?)
}

/**
 * Checks if the [value]-collection contains the property value.
 *
 * The in-memory matching is performed based on Collections.contains, other data providers may specify different
 * criteria for equality.
 * @param value The collection to check against, must not be empty.
 */
public data class InFilter<T : Any>(
        override val propertyName: DataLoaderPropertyName,
        override val value: Collection<Comparable<*>>
) : BeanFilter<T>() {
    init {
        check(value.isNotEmpty()) { "value: cannot be empty" }
    }

    override fun test(t: T): Boolean = value.contains(getValue(t))
    override fun toString(): String =
            value.joinToString(", ", prefix = "$propertyName in (", postfix = ")") { "'$it'" }
}

/**
 * Checks that property value is null.
 */
public data class IsNullFilter<T : Any>(override val propertyName: DataLoaderPropertyName) : BeanFilter<T>() {
    override val value: Any? = null
    override fun test(t: T): Boolean = getValue(t) == null
    override fun toString(): String = "$propertyName IS NULL"
}

/**
 * Checks that the property value is not null.
 *
 * *Implementation detail*: [value] returns null even though this filter matches non-null values.
 */
public data class IsNotNullFilter<T : Any>(override val propertyName: DataLoaderPropertyName) : BeanFilter<T>() {
    /**
     * Do not rely on the value in this particular case; it's impossible to represent
     * an universal set with a finite sets provided by Kotlin.
     */
    override val value: Any? = null
    override fun test(t: T): Boolean = getValue(t) != null
    override fun toString(): String = "$propertyName IS NOT NULL"
}

/**
 * A starts-with filter. It performs the 'starts-with' matching, case-sensitive depending
 * on the value of [ignoreCase].
 *
 * SQL: This filter emits a statement `prop LIKE 'startsWith%'` and automatically
 * appends `%` to [startsWith]. This kind of query tends to perform quite well on indexed columns.
 *
 * See [SubstringFilter] for substring matching, but beware of its poor performance on the database.
 * @param startsWith the prefix. Non-string values are always filtered out.
 * @property ignoreCase if false the string is matched case-sensitive. Defaults to true.
 */
public class StartsWithFilter<T : Any>(
        override val propertyName: DataLoaderPropertyName,
        startsWith: String,
        public val ignoreCase: Boolean = true
) : BeanFilter<T>() {
    public val startsWith: String = startsWith.trim()
    override val value: String get() = startsWith
    override fun toString(): String = "$propertyName ${if (ignoreCase) "i" else ""}starts-with $formattedValue"
    override fun equals(other: Any?): Boolean {
        if (this === other) return true
        if (other?.javaClass != javaClass) return false
        other as StartsWithFilter<*>
        if (propertyName != other.propertyName) return false
        if (ignoreCase != other.ignoreCase) return false
        if (value != other.value) return false
        return true
    }

    override fun hashCode(): Int {
        var result: Int = propertyName.hashCode()
        result = 31 * result + value.hashCode()
        result = 31 * result + ignoreCase.hashCode()
        return result
    }

    override fun test(t: T): Boolean {
        val v: String = getValue(t) as? String ?: return false
        return v.startsWith(startsWith, ignoreCase)
    }
}

@Deprecated("Use StartsWithFilter", ReplaceWith("StartsWithFilter<T>"))
public typealias ILikeFilter<T> = StartsWithFilter<T>

/**
 * NOT filter: negates the result of [child]
 * @param child child filter
 */
public class NotFilter<T : Any>(public val child: Filter<in T>) : Filter<T> {
    override fun toString(): String = "not ($child)"
    override fun test(t: T): Boolean = !child.test(t)
    override fun equals(other: Any?): Boolean {
        if (this === other) return true
        if (javaClass != other?.javaClass) return false
        other as NotFilter<*>
        if (child != other.child) return false
        return true
    }

    override fun hashCode(): Int = child.hashCode()
}

/**
 * AND filter: matches item only when all [children] matches.
 * @param children child filters, must not be empty.
 */
public class AndFilter<T : Any>(children: Set<Filter<in T>>) : Filter<T> {
    init {
        check(children.isNotEmpty()) { "children: cannot be empty" }
    }

    public val children: Set<Filter<in T>> = children.flatMap { if (it is AndFilter) it.children else listOf(it) }.toSet()
    override fun toString(): String = children.joinToString(" and ", "(", ")")
    override fun equals(other: Any?): Boolean {
        if (this === other) return true
        if (other?.javaClass != javaClass) return false
        other as AndFilter<*>
        if (children != other.children) return false
        return true
    }

    override fun hashCode(): Int = children.hashCode()
    override fun test(t: T): Boolean = children.all { it.test(t) }
}

/**
 * OR filter: matches item only when at least one of [children] matches.
 * @param children child filters, must not be empty.
 */
public class OrFilter<T : Any>(children: Set<Filter<in T>>) : Filter<T> {
    init {
        check(children.isNotEmpty()) { "children: cannot be empty" }
    }

    public val children: Set<Filter<in T>> = children.flatMap { if (it is OrFilter) it.children else listOf(it) }.toSet()
    override fun toString(): String = children.joinToString(" or ", "(", ")")
    override fun equals(other: Any?): Boolean {
        if (this === other) return true
        if (other?.javaClass != javaClass) return false
        other as OrFilter<*>
        if (children != other.children) return false
        return true
    }

    override fun hashCode(): Int = children.hashCode()
    override fun test(t: T): Boolean = children.any { it.test(t) }
}

/**
 * ANDs given set of filters and produces an [AndFilter].
 * @return ANDed filters; `null` when receiver is empty; if receiver contains exactly one filter, that filter is simply returned.
 */
public fun <T : Any> Set<Filter<T>>.and(): Filter<T>? = when (size) {
    0 -> null
    1 -> first()
    else -> AndFilter(this)
}

/**
 * ORs given set of filters and produces an [OrFilter].
 * @return ORed filters; `null` when receiver is empty; if receiver contains exactly one filter, that filter is simply returned.
 */
public fun <T : Any> Set<Filter<T>>.or(): Filter<T>? = when (size) {
    0 -> null
    1 -> first()
    else -> OrFilter(this)
}

/**
 * Just write any native SQL into [where], e.g. `age > 25 and name like :name`; don't forget to properly fill in the [params] map.
 *
 * Does not support in-memory filtering and will throw an exception.
 */
public data class NativeSqlFilter<T : Any>(val where: String, val params: Map<String, Any?>) : Filter<T> {
    override fun test(t: T): Boolean = throw UnsupportedOperationException("Does not support in-memory filtering")
    override fun toString(): String = "$where$params"
}

/**
 * A LIKE filter which performs the 'substring' matching. Usually only used for in-memory
 * filtering since it performs quite poorly in the database. Case-sensitivity depends on
 * the [ignoreCase] parameter.
 *
 * *WARNING:* The database performance is very poor, even on indexed columns - the database effectively performs full
 * table scan. Instead you should use the [FullTextFilter] and the full text search
 * capabilities of your database; alternatively use [NativeSqlFilter] to write a query suited
 * for the database of your choice. For example see [PostgreSQL full-text search](https://www.postgresql.org/docs/9.5/static/textsearch.html).
 * @param contains the substring, automatically prepended and appended with `%` when the SQL query is constructed. The 'substring' is matched
 * case-sensitive.
 * @property ignoreCase if false the string is matched case-sensitive. Defaults to true.
 */
public class SubstringFilter<T : Any>(
        override val propertyName: DataLoaderPropertyName,
        contains: String,
        public val ignoreCase: Boolean = true
) : BeanFilter<T>() {
    /**
     * Any probe must contain this string.
     */
    public val contains: String = contains.trim()
    override val value: String get() = contains
    override fun toString(): String = "$propertyName ${if (ignoreCase) "i" else ""}contains $formattedValue"
    override fun equals(other: Any?): Boolean {
        if (this === other) return true
        if (other?.javaClass != javaClass) return false
        other as SubstringFilter<*>
        if (propertyName != other.propertyName) return false
        if (ignoreCase != other.ignoreCase) return false
        if (value != other.value) return false
        return true
    }

    override fun hashCode(): Int {
        var result: Int = propertyName.hashCode()
        result = 31 * result + value.hashCode()
        result = 31 * result + ignoreCase.hashCode()
        return result
    }

    override fun test(t: T): Boolean {
        val v: String = getValue(t) as? String ?: return false
        return v.contains(contains, ignoreCase)
    }
}

/**
 * A FullText filter which performs the case-insensitive full-text search.
 * Any probe text must either contain all words in this query,
 * or the query words must match beginnings of all of the words contained in the probe string.
 *
 * Different implementors will implement this differently and may have different
 * requirements on how to configure the storage for full-text search:
 * * For example to have a full-text search in an SQL database/RDBMS system please use the
 *   [vok-orm](https://github.com/mvysny/vok-orm) library and read the
 *   [Full-Text Filters chapter](https://github.com/mvysny/vok-orm#full-text-filters).
 * @param query any probe text must either contain words in this query,
 * or the query words must match beginnings of the words contained in the probe.
 */
public class FullTextFilter<T : Any>(override val propertyName: DataLoaderPropertyName, query: String) : BeanFilter<T>() {
    /**
     * In order for the probe to match, the probe must either match these words,
     * or the query words must match beginnings of the words contained in the probe.
     */
    public val words: Set<String> = query.trim().lowercase().splitToWords().toSet()
    override val value: String get() = words.toString()
    override fun toString(): String = "$propertyName ~ $words"
    override fun equals(other: Any?): Boolean {
        if (this === other) return true
        if (other?.javaClass != javaClass) return false
        other as FullTextFilter<*>
        if (propertyName != other.propertyName) return false
        if (words != other.words) return false
        return true
    }

    override fun hashCode(): Int {
        var result: Int = propertyName.hashCode()
        result = 31 * result + value.hashCode()
        return result
    }

    override fun test(t: T): Boolean {
        val v: String = getValue(t) as? String ?: return false
        if (words.isEmpty()) {
            return true
        }
        val probe: Set<String> = v.trim().lowercase().splitToWords().toSet()
        for (word: String in words) {
            if (!probe.contains(word) && probe.none { it.startsWith(word) }) {
                return false
            }
        }
        return true
    }
}
