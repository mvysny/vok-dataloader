package com.github.mvysny.vokdataloader

/**
 * Returns a new data loader which always applies given [filter] and ANDs it with
 * any filter given to [DataLoader.getCount] or [DataLoader.fetch].
 */
public fun <T : Any> DataLoader<T>.withFilter(filter: Filter<T>): DataLoader<T> = FilteredDataLoader(filter, this)

/**
 * Returns a new data loader which always applies given [filter] and ANDs it with
 * any filter given to [DataLoader.getCount] or [DataLoader.fetch].
 */
public inline fun <reified T : Any> DataLoader<T>.withFilter(block: FilterBuilder<T>.() -> Filter<T>): DataLoader<T> =
        withFilter(FilterBuilder(T::class.java).block())

/**
 * Wraps [delegate] data loader and always applies given [filter] and ANDs it
 * with any filter given to [DataLoader.getCount] or [DataLoader.fetch].
 *
 * You can use [withFilter] convenience methods to produce instances of this class.
 *
 * @property filter unremovable filters, always applied when [getCount] or [fetch] is called.
 * @property delegate underlying [DataLoader] which performs the actual fetching..
 */
public class FilteredDataLoader<T : Any>(
        public val filter: Filter<T>,
        public val delegate: DataLoader<T>
) : DataLoader<T> {
    private fun and(other: Filter<T>?): Filter<T> = if (other == null) filter else filter.and(other)

    override fun getCount(filter: Filter<T>?): Long = delegate.getCount(and(filter))

    override fun fetch(filter: Filter<T>?, sortBy: List<SortClause>, range: LongRange): List<T> =
            delegate.fetch(and(filter), sortBy, range)

    override fun toString(): String = "FilteredDataLoader(filter=$filter, $delegate)"
}
