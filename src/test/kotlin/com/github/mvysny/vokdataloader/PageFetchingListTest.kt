package com.github.mvysny.vokdataloader

import com.github.mvysny.dynatest.DynaTest
import com.github.mvysny.dynatest.expectList
import com.github.mvysny.dynatest.expectThrows
import java.lang.IndexOutOfBoundsException
import kotlin.test.expect

class PageFetchingListTest : DynaTest({
    test("empty") {
        expectList() { EmptyDataLoader<Int>().asList(5).iterator().asSequence().toList() }
        expect(0) { EmptyDataLoader<Int>().asList(5).size }
        expect(true) { EmptyDataLoader<Int>().asList(5).isEmpty() }
    }

    test("simple iteration") {
        val list = (0..10).toList()
        expect(list) { list.dataLoader().asList(3).toList() }
    }

    test("long iteration") {
        val list = (0..1000).toList()
        expect(list) { list.dataLoader().asList(7).toList() }
    }

    test("keeps at most three pages") {
        val list = PageFetchingList((0..1000).toList().dataLoader(), 70)
        for (i in 0..1000) {
            expect(i) { list[i] }
            expect(true, "${list.cache}") { list.cache.size <= 3 }
        }
    }

    test("out of bounds") {
        val list = PageFetchingList((0..1000).toList().dataLoader(), 70)
        expectThrows(IndexOutOfBoundsException::class) {
            list[-1]
        }
        expectThrows(IndexOutOfBoundsException::class, "Index 1001 resolved to page 14 offset 21 but that page contains less than 70 items: 21. Maybe the dataset has been changed. Reported total number of items: 1001") {
            list[1001]
        }
        expectThrows(IndexOutOfBoundsException::class, "Index 100000 resolved to page 1428 but fetching prev page yielded less than 70 items: 0. Maybe the dataset has been changed. Reported total number of items: 1001") {
            list[100000]
        }
    }

    test("buffer item") {
        val list: PageFetchingList<Int> = PageFetchingList((0..1000).toList().dataLoader(), 70, -1)
        expectThrows(IndexOutOfBoundsException::class) {
            list[-1]
        }
        expect(-1) { list[1001] }
        expect(-1) { list[100000] }
    }

    test("toString()") {
        var list: PageFetchingList<Int> = PageFetchingList((0..1000).toList().dataLoader(), 70, -1)
        expect("PageFetchingList(loader=ListDataLoader{1001 items, first three items: [0, 1, 2]}, pageSize=70, dataMissingMarker=-1)") {
            list.toString()
        }
        list = PageFetchingList(EmptyDataLoader(), 20, -1)
        expect("PageFetchingList(loader=EmptyDataLoader, pageSize=20, dataMissingMarker=-1)") {
            list.toString()
        }
    }
})