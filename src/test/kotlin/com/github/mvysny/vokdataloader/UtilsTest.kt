package com.github.mvysny.vokdataloader

import com.github.mvysny.dynatest.DynaTest
import com.github.mvysny.dynatest.expectList
import java.lang.reflect.Method
import java.math.BigDecimal
import kotlin.test.expect

class UtilsTest : DynaTest({
    test("subList") {
        expectList(1, 2, 3) { listOf(1, 2, 3).subList(0..2) }
        expectList(2, 3) { listOf(1, 2, 3).subList(1..2) }
        expectList(1, 2) { listOf(1, 2, 3).subList(0..1) }
        expectList() { listOf(1, 2, 3).subList(IntRange.EMPTY) }
        expectList() { listOf<Int>().subList(IntRange.EMPTY) }
    }

    test("splitToWords") {
        expectList() { "".splitToWords() }
        expectList() { "   ".splitToWords() }
        expectList("what") { "  what?  ".splitToWords() }
        expectList("what", "are", "you", "suggesting") { "  what   are you suggesting?  ".splitToWords() }
        expectList("l'oreal") { "l'oreal".splitToWords() }
        expectList("私", "は", "彼女", "をかわいらしいと", "思", "った") { "私は彼女をかわいらしいと思った。".splitToWords() }
    }

    test("toBigDecimal()") {
        expect(BigDecimal.ZERO) { 0.toBigDecimal() }
        expect(BigDecimal("0.0")) { 0.0.toBigDecimal() }
        expect(BigDecimal.ZERO) { 0.toShort().toBigDecimal() }
        expect(BigDecimal("0.0")) { 0f.toBigDecimal() }
        expect(BigDecimal.ZERO) { 0.toByte().toBigDecimal() }
        expect(BigDecimal.ZERO) { 0L.toBigDecimal() }
    }

    group("getGetter()") {
        // https://gitlab.com/mvysny/vok-dataloader/-/issues/7
        test("inline class") {
            val getter: Method = MyResults::class.java.getGetter("id")
            expect(String::class.java) { getter.returnType }
            val r = MyResults()
            expect("testId") { getter.invoke(r) }
        }
    }
})

@JvmInline
value class MyId(val id: String = "")
data class MyResults(val id: MyId = MyId("testId"))
