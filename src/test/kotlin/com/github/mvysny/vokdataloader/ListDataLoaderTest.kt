package com.github.mvysny.vokdataloader

import com.github.mvysny.dynatest.DynaTest
import com.github.mvysny.dynatest.expectList
import com.github.mvysny.dynatest.expectThrows
import java.lang.IllegalArgumentException
import java.math.BigInteger
import kotlin.test.expect

class ListDataLoaderTest : DynaTest({
    group("empty list") {
        test("count") {
            val dl: ListDataLoader<Person> = ListDataLoader(Person::class.java, listOf())
            expect(0) { dl.getCount(null) }
            expect(0) { dl.getCount(buildFilter { Person::name eq "John" }) }
        }

        test("fetch") {
            val dl = ListDataLoader(Person::class.java, listOf())
            expectList() { dl.fetch() }
            expectList() { dl.fetch(range = LongRange.EMPTY) }
            expectList() { dl.fetch(range = 5000L..6000L) }
            expectList() { dl.fetch(buildFilter { Person::name eq "John" }) }
            expectList() { dl.fetch(sortBy = listOf(Person::name.asc)) }
            expectList() { dl.fetch(buildFilter { Person::name eq "John" }, range = 5000L..6000L) }
            expectList() { dl.fetch(sortBy = listOf(Person::name.asc), range = 5000L..6000L) }
            expectList() { dl.fetch(buildFilter { Person::name eq "John" }, sortBy = listOf(Person::name.asc), range = 5000L..6000L) }
        }

        test("fetch with negative indices") {
            expectThrows(IllegalArgumentException::class, "range -30..-1 contains negative indices") {
                ListDataLoader(Person::class.java, listOf()).fetch(range = -30L..-1L)
            }
        }

        test("property path") {
            val dl = ListDataLoader(OneManCompany::class.java, listOf())
            expect(0) { dl.getCount(EqFilter("employee.person.name", "John")) }
        }
    }

    group("10 items") {
        test("count") {
            val dl = ListDataLoader(Person::class.java, (0..9).map { Person("name $it") })
            expect(10) { dl.getCount(null) }
            expect(0) { dl.getCount(buildFilter { Person::name eq "John" }) }
            expect(1) { dl.getCount(buildFilter { Person::name eq "name 5" }) }
            expect(10) { dl.getCount(buildFilter { Person::name istartsWith "Name" }) }
        }

        test("fetch") {
            val list = (0..9).map { Person("name $it") }
            val dl = ListDataLoader(Person::class.java, list)
            expect(list) { dl.fetch() }
            expectList() { dl.fetch(range = 5000L..6000L) }
            expectList() { dl.fetch(range = LongRange.EMPTY) }
            expectList(list[5]) { dl.fetch(buildFilter { Person::name eq "name 5" }) }
            expect(list) { dl.fetch(sortBy = listOf(Person::name.asc)) }
            expect(list.reversed()) { dl.fetch(sortBy = listOf(Person::name.desc)) }
            expectList() { dl.fetch(buildFilter { Person::name eq "name 5" }, range = 5000L..6000L) }
            expectList(list[5]) { dl.fetch(buildFilter { Person::name eq "name 5" }, range = 0L..3L) }
            expectList() { dl.fetch(sortBy = listOf(Person::name.asc), range = 5000L..6000L) }
            expect(list.subList(0, 4)) { dl.fetch(sortBy = listOf(Person::name.asc), range = 0L..3L) }
            expectList(list[5]) { dl.fetch(buildFilter { Person::name eq "name 5" }, sortBy = listOf(Person::name.asc), range = 0L..2L) }
            expect(list.subList(3, 5)) { dl.fetch(buildFilter { Person::name istartsWith "name" }, sortBy = listOf(Person::name.asc), range = 3L..4L) }
        }

        test("property path") {
            val dl = ListDataLoader(OneManCompany::class.java, (0..9).map { OneManCompany(Employee(Person("name $it"))) })
            expect(1) { dl.getCount(EqFilter("employee.person.name", "name 3")) }
        }
    }

    group("null items") {
        test("count") {
            val dl = ListDataLoader(Person::class.java, (0..9).map { Person(null) })
            expect(10) { dl.getCount(null) }
            expect(0) { dl.getCount(buildFilter { Person::name eq "John" }) }
            expect(10) { dl.getCount(buildFilter { Person::name.isNull }) }
        }

        test("fetch") {
            val list = (0..9).map { Person(null) }
            val dl = ListDataLoader(Person::class.java, list)
            expect(list) { dl.fetch() }
            expectList() { dl.fetch(range = 5000L..6000L) }
            expectList() { dl.fetch(range = LongRange.EMPTY) }
            expect(list) { dl.fetch(buildFilter { Person::name.isNull }) }
            expect(list) { dl.fetch(sortBy = listOf(Person::name.asc)) }
            expect(list) { dl.fetch(sortBy = listOf(Person::name.desc)) }
            expectList() { dl.fetch(buildFilter { Person::name eq "name 5" }, range = 5000L..6000L) }
            expect(list.subList(0, 4)) { dl.fetch(buildFilter { Person::name.isNull }, range = 0L..3L) }
            expectList() { dl.fetch(sortBy = listOf(Person::name.asc), range = 5000L..6000L) }
            expect(list.subList(0, 4)) { dl.fetch(sortBy = listOf(Person::name.asc), range = 0L..3L) }
        }
    }

    group("sorting") {
        test("empty list") {
            expectList() { listOf<String>().sortedBy(String::length.asc) }
        }
        test("empty sorting criteria does nothing") {
            expectList("aa", "a", "aaaa", "aaa") {
                listOf("aa", "a", "aaaa", "aaa").sortedBy()
            }
        }
        test("simple Person.age sorting asc") {
            expect((0..10).toList()) {
                (0..10).map { Person(null, it) } .shuffled().sortedBy(Person::age.asc).map { it.age }
            }
        }
        test("simple Person.age sorting desc") {
            expect((10 downTo 0).toList()) {
                (0..10).map { Person(null, it) } .shuffled().sortedBy(Person::age.desc).map { it.age }
            }
        }
        test("complex Person sorting") {
            expectList(0, 1, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 2, 20, 3, 4, 5, 6, 7, 8, 9) {
                (0..20).map { Person(it.toString(), it) } .shuffled()
                        .sortedBy(Person::name.asc, Person::age.asc).map { it.age }
            }
            expect((0..20).toList()) {
                (0..20).map { Person(it.toString().map { 'a' } .joinToString(""), it) } .shuffled()
                        .sortedBy(Person::name.asc, Person::age.asc).map { it.age }
            }
            expectList(9, 8, 7, 6, 5, 4, 3, 2, 1, 0, 13, 12, 11, 10) {
                (0..13).map { Person(it.toString().map { 'a' } .joinToString(""), it) } .shuffled()
                        .sortedBy(Person::name.asc, Person::age.desc).map { it.age }
            }
        }

        test("property path") {
            val dl = ListDataLoader(OneManCompany::class.java, (0..3).map { OneManCompany(Employee(Person("name $it"))) })
                    .sortedBy(SortClause("employee.person.name", false))
            expectList("name 3", "name 2", "name 1", "name 0") {
                dl.fetch().map { it.employee!!.person!!.name }
            }
        }
    }

    group("filtering") {
        test("int-typed field, double-typed filter") {
            val dl: DataLoader<Person> = (0..9).map { Person(null, it) } .dataLoader()
            expectList() { dl.fetch(buildFilter { Person::age gt 25.0 }) }
            expectList(Person(null, 0)) { dl.fetch(buildFilter { Person::age le 0.0 }) }
        }
        test("int-typed field, BigInteger-typed filter") {
            val dl: DataLoader<Person> = (0..9).map { Person(null, it) } .dataLoader()
            expectList(Person(null, 0)) { dl.fetch(buildFilter { Person::age le BigInteger.ZERO }) }
        }
    }

    group("toString()") {
        test("empty") {
            expect("ListDataLoader{0 items, first three items: []}") { ListDataLoader(String::class.java, listOf()).toString() }
        }
        test("couple of items") {
            expect("ListDataLoader{2 items, first three items: [a, b]}") {
                listOf("a", "b").dataLoader().toString()
            }
            expect("ListDataLoader{4 items, first three items: [a, b, c]}") {
                listOf("a", "b", "c", "d").dataLoader().toString()
            }
            expect("ListDataLoader{100 items, first three items: [0, 1, 2]}") {
                (0..99).toList().dataLoader().toString()
            }
        }
    }
})
