package com.github.mvysny.vokdataloader

import com.github.mvysny.dynatest.DynaTest
import com.github.mvysny.dynatest.expectList
import kotlin.test.expect

class PredicateFilterTest : DynaTest({
    test("filter") {
        val teenagerFilter = PredicateFilter<Person> { it.age in 10..19 }
        val dl = (0..100).map { Person(null, age = it) }
                .dataLoader()
                .withFilter(teenagerFilter)
        expect((10..19).toList()) { dl.fetch().map { it.age } }
    }

    test("toString()") {
        val teenagerFilter = PredicateFilter<Person> { it.age in 10..19 }
        expect("PredicateFilter(predicate=Function1<com.github.mvysny.vokdataloader.Person, java.lang.Boolean>)") { teenagerFilter.toString() }
    }
})
