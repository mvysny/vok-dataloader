package com.github.mvysny.vokdataloader

import com.github.mvysny.dynatest.DynaTest
import com.github.mvysny.dynatest.expectList
import com.github.mvysny.dynatest.expectThrows
import java.lang.IllegalArgumentException
import java.lang.RuntimeException
import kotlin.test.expect

class ChunkFetchingLoaderTest : DynaTest({
    test("fetch with negative indices") {
        expectThrows(IllegalArgumentException::class, "range -30..-1 contains negative indices") {
            ThrowingLoader<Person>().limitChunkSize(5).fetch(range = -30L..-1L)
        }
    }

    test("empty delegate") {
        val dl = EmptyDataLoader<Int>()
        expectList() { dl.limitChunkSize(1).fetch(range = 0L..9) }
        expectList() { dl.limitChunkSize(5).fetch(range = 0L..9) }
        expectList() { dl.limitChunkSize(9).fetch(range = 0L..9) }
        expectList() { dl.limitChunkSize(10).fetch(range = 0L..9) }
        expectList() { dl.limitChunkSize(20).fetch(range = 0L..9) }
        expectList() { dl.limitChunkSize(100).fetch(range = 0L..9) }
        expectList() { dl.limitChunkSize(1).fetch(range = 10L..19) }
        expectList() { dl.limitChunkSize(5).fetch(range = 10L..19) }
        expectList() { dl.limitChunkSize(9).fetch(range = 10L..19) }
        expectList() { dl.limitChunkSize(10).fetch(range = 10L..19) }
        expectList() { dl.limitChunkSize(20).fetch(range = 10L..19) }
        expectList() { dl.limitChunkSize(100).fetch(range = 10L..19) }
    }

    test("delegate with 1 item") {
        val dl = listOf(25).dataLoader()
        expectList(25) { dl.limitChunkSize(1).fetch(range = 0L..9) }
        expectList(25) { dl.limitChunkSize(5).fetch(range = 0L..9) }
        expectList(25) { dl.limitChunkSize(9).fetch(range = 0L..9) }
        expectList(25) { dl.limitChunkSize(10).fetch(range = 0L..9) }
        expectList(25) { dl.limitChunkSize(20).fetch(range = 0L..9) }
        expectList(25) { dl.limitChunkSize(100).fetch(range = 0L..9) }
        expectList() { dl.limitChunkSize(1).fetch(range = 10L..19) }
        expectList() { dl.limitChunkSize(5).fetch(range = 10L..19) }
        expectList() { dl.limitChunkSize(9).fetch(range = 10L..19) }
        expectList() { dl.limitChunkSize(10).fetch(range = 10L..19) }
        expectList() { dl.limitChunkSize(20).fetch(range = 10L..19) }
        expectList() { dl.limitChunkSize(100).fetch(range = 10L..19) }
    }

    test("delegate with 5 items") {
        val dl = listOf(0, 1, 2, 3, 4).dataLoader()
        expectList(0, 1, 2, 3, 4) { dl.limitChunkSize(1).fetch(range = 0L..9) }
        expectList(0, 1, 2, 3, 4) { dl.limitChunkSize(5).fetch(range = 0L..9) }
        expectList(0, 1, 2, 3, 4) { dl.limitChunkSize(9).fetch(range = 0L..9) }
        expectList(0, 1, 2, 3, 4) { dl.limitChunkSize(10).fetch(range = 0L..9) }
        expectList(0, 1, 2, 3, 4) { dl.limitChunkSize(20).fetch(range = 0L..9) }
        expectList(0, 1, 2, 3, 4) { dl.limitChunkSize(100).fetch(range = 0L..9) }
        expectList() { dl.limitChunkSize(1).fetch(range = 10L..19) }
        expectList() { dl.limitChunkSize(5).fetch(range = 10L..19) }
        expectList() { dl.limitChunkSize(9).fetch(range = 10L..19) }
        expectList() { dl.limitChunkSize(10).fetch(range = 10L..19) }
        expectList() { dl.limitChunkSize(20).fetch(range = 10L..19) }
        expectList() { dl.limitChunkSize(100).fetch(range = 10L..19) }
    }

    test("delegate with 20 items") {
        val dl = (0..19).toList().dataLoader()
        expect((0..9).toList()) { dl.limitChunkSize(1).fetch(range = 0L..9) }
        expect((0..9).toList()) { dl.limitChunkSize(5).fetch(range = 0L..9) }
        expect((0..9).toList()) { dl.limitChunkSize(9).fetch(range = 0L..9) }
        expect((0..9).toList()) { dl.limitChunkSize(10).fetch(range = 0L..9) }
        expect((0..9).toList()) { dl.limitChunkSize(20).fetch(range = 0L..9) }
        expect((0..9).toList()) { dl.limitChunkSize(100).fetch(range = 0L..9) }
        expect((10..19).toList()) { dl.limitChunkSize(1).fetch(range = 10L..19) }
        expect((10..19).toList()) { dl.limitChunkSize(5).fetch(range = 10L..19) }
        expect((10..19).toList()) { dl.limitChunkSize(9).fetch(range = 10L..19) }
        expect((10..19).toList()) { dl.limitChunkSize(10).fetch(range = 10L..19) }
        expect((10..19).toList()) { dl.limitChunkSize(20).fetch(range = 10L..19) }
        expect((10..19).toList()) { dl.limitChunkSize(100).fetch(range = 10L..19) }
    }
})

class ThrowingLoader<T: Any> : DataLoader<T> {
    override fun getCount(filter: Filter<T>?): Long = throw RuntimeException("simulated")
    override fun fetch(filter: Filter<T>?, sortBy: List<SortClause>, range: LongRange): List<T> = throw RuntimeException("simulated")
}