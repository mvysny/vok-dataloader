package com.github.mvysny.vokdataloader

import com.github.mvysny.dynatest.DynaTest
import kotlin.test.assertFailsWith
import kotlin.test.expect

class FiltersTest : DynaTest({
    test("not") {
        val eq = EqFilter<Person>(Person::name.name, "Name")
        val not = NotFilter(eq)

        expect(true) { not.test(Person("Name 5")) }
        expect(false) { not.test(Person("Name")) }

        expect("not (name = 'Name')") { not.toString() }
        expect(true) { not == NotFilter(EqFilter<Person>(Person::name.name, "Name")) }
        expect(false) { not == NotFilter(EqFilter<Person>(Person::name.name, "namea")) }
    }

    test("in") {
        val in1 = InFilter<Person>(Person::name.name, listOf("Name", "name 5"))
        expect("name in ('Name', 'name 5')") { in1.toString() }
        expect(true) { in1.test(Person("Name")) }
        expect(true) { in1.test(Person("name 5")) }
        expect(false) { in1.test(Person("name")) }
        expect(false) { in1.test(Person("Name 5")) }
        expect(false) { in1.test(Person("Na")) }
        expect(false) { in1.test(Person("na")) }
        expect(false) { in1.test(Person("")) }
        expect(false) { in1.test(Person(null)) }

        val in2 = InFilter<Person>(Person::name.name, listOf("name"))
        expect("name in ('name')") { in2.toString() }
        expect(true) { in2.test(Person("name")) }
        expect(false) { in2.test(Person("name 5")) }
        expect(false) { in2.test(Person("Name")) }
        expect(false) { in2.test(Person("Na")) }
        expect(false) { in2.test(Person("na")) }
        expect(false) { in1.test(Person(null)) }

        assertFailsWith(IllegalStateException::class) {
            InFilter<Person>(Person::name.name, listOf())
        }
        expect(true) { in1 == InFilter<Person>(Person::name.name, listOf("Name", "name 5")) }
        expect(false) { in1 == InFilter<Person>(Person::name.name, listOf("Name", "name 6")) }
    }

    test("startswith") {
        val like = StartsWithFilter<Person>(Person::name.name, "Name", false)
        expect(false) { like.test(Person("name 5")) }
        expect(false) { like.test(Person("name")) }
        expect(true) { like.test(Person("Name 5")) }
        expect(true) { like.test(Person("Name")) }
        expect(false) { like.test(Person("5 Name 5")) }
        expect(false) { like.test(Person("5 name 5")) }
        expect(false) { like.test(Person("Na")) }
        expect(false) { like.test(Person("na")) }
        expect(false) { like.test(Person("")) }
        expect(false) { like.test(Person(null)) }
        expect("name starts-with 'Name'") { like.toString() }
        expect(true) { like == StartsWithFilter<Person>(Person::name.name, "Name", false) }
        expect(false) { like == StartsWithFilter<Person>(Person::name.name, "Name") }
        expect(false) { like == StartsWithFilter<Person>(Person::name.name, "namea") }
    }

    test("istartswith") {
        val filter = StartsWithFilter<Person>(Person::name.name, "Name")
        expect(true) { filter.test(Person("name 5")) }
        expect(true) { filter.test(Person("name")) }
        expect(true) { filter.test(Person("Name 5")) }
        expect(true) { filter.test(Person("Name")) }
        expect(false) { filter.test(Person("5 Name 5")) }
        expect(false) { filter.test(Person("5 name 5")) }
        expect(false) { filter.test(Person("Na")) }
        expect(false) { filter.test(Person("na")) }
        expect(false) { filter.test(Person("")) }
        expect(false) { filter.test(Person(null)) }
        expect("name istarts-with 'Name'") { filter.toString() }
        expect(true) { filter == StartsWithFilter<Person>(Person::name.name, "Name") }
        expect(false) { filter == StartsWithFilter<Person>(Person::name.name, "namea") }
    }

    test("substring") {
        val like = SubstringFilter<Person>(Person::name.name, "Name", false)
        expect(false) { like.test(Person("name 5")) }
        expect(false) { like.test(Person("name")) }
        expect(true) { like.test(Person("Name 5")) }
        expect(true) { like.test(Person("Name")) }
        expect(true) { like.test(Person("5 Name 5")) }
        expect(false) { like.test(Person("5 name 5")) }
        expect(false) { like.test(Person("Na")) }
        expect(false) { like.test(Person("na")) }
        expect(false) { like.test(Person("")) }
        expect(false) { like.test(Person(null)) }
        expect("name contains 'Name'") { like.toString() }
        expect(true) { like == SubstringFilter<Person>(Person::name.name, "Name", false) }
        expect(false) { like == SubstringFilter<Person>(Person::name.name, "Name") }
        expect(false) { like == SubstringFilter<Person>(Person::name.name, "namea") }
    }

    test("isubstring") {
        val filter = SubstringFilter<Person>(Person::name.name, "Name")
        expect(true) { filter.test(Person("name 5")) }
        expect(true) { filter.test(Person("name")) }
        expect(true) { filter.test(Person("Name 5")) }
        expect(true) { filter.test(Person("Name")) }
        expect(true) { filter.test(Person("5 Name 5")) }
        expect(true) { filter.test(Person("5 name 5")) }
        expect(false) { filter.test(Person("Na")) }
        expect(false) { filter.test(Person("na")) }
        expect(false) { filter.test(Person("")) }
        expect(false) { filter.test(Person(null)) }
        expect("name icontains 'Name'") { filter.toString() }
        expect(true) { filter == SubstringFilter<Person>(Person::name.name, "Name") }
        expect(false) { filter == SubstringFilter<Person>(Person::name.name, "namea") }
    }

    test("fulltext") {
        fun matches(query: String) = FullTextFilter<Person>(Person::name.name, query).test(Person("An SQL JOIN clause combines rows from two or more tables. It creates a set of rows in a temporary table."))
        expect(true) { matches("a sql") }
        expect(true) { matches("an sql") }
        expect(true) { matches("joi") }
        expect(true) { matches("create combine") }
        expect(true) { matches("") }
        expect(false) { matches("person") }
        expect(false) { matches("join person") }
        expect(false) { matches("sql person join") }
        val ft = FullTextFilter<Person>(Person::name.name, "Name")
        expect("name ~ [name]") { ft.toString() }
        expect(true) { ft == FullTextFilter<Person>(Person::name.name, "Name") }
        expect(false) { ft == FullTextFilter<Person>(Person::name.name, "namea") }
    }

    test("isnull") {
        val isnull = IsNullFilter<Person>(Person::name.name)
        expect(false) { isnull.test(Person("name 5")) }
        expect(false) { isnull.test(Person("name")) }
        expect(false) { isnull.test(Person("Name 5")) }
        expect(false) { isnull.test(Person("Name")) }
        expect(false) { isnull.test(Person("Na")) }
        expect(false) { isnull.test(Person("na")) }
        expect(false) { isnull.test(Person("")) }
        expect(true) { isnull.test(Person(null)) }
        expect("name IS NULL") { isnull.toString() }
        expect(true) { isnull == IsNullFilter<Person>(Person::name.name) }
        expect(false) { isnull == IsNullFilter<Person>("surname") }
    }

    test("isnotnull") {
        val isnull = IsNotNullFilter<Person>(Person::name.name)
        expect(true) { isnull.test(Person("name 5")) }
        expect(true) { isnull.test(Person("name")) }
        expect(true) { isnull.test(Person("Name 5")) }
        expect(true) { isnull.test(Person("Name")) }
        expect(true) { isnull.test(Person("Na")) }
        expect(true) { isnull.test(Person("na")) }
        expect(true) { isnull.test(Person("")) }
        expect(false) { isnull.test(Person(null)) }
        expect("name IS NOT NULL") { isnull.toString() }
        expect(true) { isnull == IsNotNullFilter<Person>(Person::name.name) }
        expect(false) { isnull == IsNotNullFilter<Person>("surname") }
    }

    test("eq") {
        val eq = EqFilter<Person>(Person::name.name, "Name")
        expect(false) { eq.test(Person("name 5")) }
        expect(false) { eq.test(Person("name")) }
        expect(false) { eq.test(Person("Name 5")) }
        expect(true) { eq.test(Person("Name")) }
        expect(false) { eq.test(Person("Na")) }
        expect(false) { eq.test(Person("na")) }
        expect(false) { eq.test(Person("")) }
        expect(false) { eq.test(Person(null)) }
        expect("name = 'Name'") { eq.toString() }
        expect(true) { eq == EqFilter<Person>(Person::name.name, "Name") }
        expect(false) { eq == EqFilter<Person>(Person::name.name, "namea") }
    }

    group("op") {
        test("eq") {
            val eq = OpFilter<Person>(Person::name.name, "Name", CompareOperator.eq)
            expect(false) { eq.test(Person("name 5")) }
            expect(false) { eq.test(Person("name")) }
            expect(false) { eq.test(Person("Name 5")) }
            expect(true) { eq.test(Person("Name")) }
            expect(false) { eq.test(Person("Na")) }
            expect(false) { eq.test(Person("na")) }
            expect(false) { eq.test(Person("")) }
            expect(false) { eq.test(Person(null)) }
            expect("name = 'Name'") { eq.toString() }
            expect(true) { eq == OpFilter<Person>(Person::name.name, "Name", CompareOperator.eq) }
            expect(false) { eq == OpFilter<Person>(Person::name.name, "namea", CompareOperator.eq) }
            expect(false) { eq == OpFilter<Person>(Person::name.name, "Name", CompareOperator.ge) }
        }
        test("lt") {
            val eq = OpFilter<Person>(Person::name.name, "Name", CompareOperator.lt)
            expect(false) { eq.test(Person("name 5")) }
            expect(false) { eq.test(Person("name")) }
            expect(false) { eq.test(Person("Name 5")) }
            expect(false) { eq.test(Person("Name")) }
            expect(true) { eq.test(Person("Na")) }
            expect(false) { eq.test(Person("na")) }
            expect(true) { eq.test(Person("")) }
            expect(true) { eq.test(Person(null)) }
            expect("name < 'Name'") { eq.toString() }
            expect(true) { eq == OpFilter<Person>(Person::name.name, "Name", CompareOperator.lt) }
            expect(false) { eq == OpFilter<Person>(Person::name.name, "namea", CompareOperator.lt) }
            expect(false) { eq == OpFilter<Person>(Person::name.name, "Name", CompareOperator.eq) }
        }
        test("le") {
            val eq = OpFilter<Person>(Person::name.name, "Name", CompareOperator.le)
            expect(false) { eq.test(Person("name 5")) }
            expect(false) { eq.test(Person("name")) }
            expect(false) { eq.test(Person("Name 5")) }
            expect(true) { eq.test(Person("Name")) }
            expect(true) { eq.test(Person("Na")) }
            expect(false) { eq.test(Person("na")) }
            expect(true) { eq.test(Person("")) }
            expect(true) { eq.test(Person(null)) }
            expect("name <= 'Name'") { eq.toString() }
            expect(true) { eq == OpFilter<Person>(Person::name.name, "Name", CompareOperator.le) }
            expect(false) { eq == OpFilter<Person>(Person::name.name, "namea", CompareOperator.le) }
            expect(false) { eq == OpFilter<Person>(Person::name.name, "Name", CompareOperator.eq) }
        }
        test("ge") {
            val eq = OpFilter<Person>(Person::name.name, "Name", CompareOperator.ge)
            expect(true) { eq.test(Person("name 5")) }
            expect(true) { eq.test(Person("name")) }
            expect(true) { eq.test(Person("Name 5")) }
            expect(true) { eq.test(Person("Name")) }
            expect(false) { eq.test(Person("Na")) }
            expect(true) { eq.test(Person("na")) }
            expect(false) { eq.test(Person("")) }
            expect(false) { eq.test(Person(null)) }
            expect("name >= 'Name'") { eq.toString() }
            expect(true) { eq == OpFilter<Person>(Person::name.name, "Name", CompareOperator.ge) }
            expect(false) { eq == OpFilter<Person>(Person::name.name, "namea", CompareOperator.ge) }
            expect(false) { eq == OpFilter<Person>(Person::name.name, "Name", CompareOperator.eq) }
        }
        test("gt") {
            val eq = OpFilter<Person>(Person::name.name, "Name", CompareOperator.gt)
            expect(true) { eq.test(Person("name 5")) }
            expect(true) { eq.test(Person("name")) }
            expect(true) { eq.test(Person("Name 5")) }
            expect(false) { eq.test(Person("Name")) }
            expect(false) { eq.test(Person("Na")) }
            expect(true) { eq.test(Person("na")) }
            expect(false) { eq.test(Person("")) }
            expect(false) { eq.test(Person(null)) }
            expect("name > 'Name'") { eq.toString() }
            expect(true) { eq == OpFilter<Person>(Person::name.name, "Name", CompareOperator.gt) }
            expect(false) { eq == OpFilter<Person>(Person::name.name, "namea", CompareOperator.gt) }
            expect(false) { eq == OpFilter<Person>(Person::name.name, "Name", CompareOperator.eq) }
        }
    }

    group("build filter") {
        test("bean filters") {
            expect("name = '5'") { buildFilter<Person> { Person::name eq 5 } .toString() }
            expect("name < '5'") { buildFilter<Person> { Person::name lt 5 } .toString() }
            expect("name > '5'") { buildFilter<Person> { Person::name gt 5 } .toString() }
            expect("name >= '5'") { buildFilter<Person> { Person::name ge 5 } .toString() }
            expect("name istarts-with 'foo'") { buildFilter<Person> { Person::name istartsWith "foo" } .toString() }
            expect("name starts-with 'foo'") { buildFilter<Person> { Person::name startsWith "foo" } .toString() }
            expect("name >= :foo{foo=foo}") { buildFilter<Person> { "name >= :foo"("foo" to "foo") } .toString() }
            expect("name contains 'foo'") { buildFilter<Person> { Person::name contains "foo" } .toString() }
            expect("name icontains 'bar'") { buildFilter<Person> { Person::name icontains "bar" } .toString() }
            expect("age in ('15', '20', '25')") { buildFilter<Person> { Person::age `in` setOf(15, 20, 25) } .toString() }
            expect("age <> '15'") { buildFilter<Person> { Person::age ne 15 } .toString() }
            expect("name IS NOT NULL") { buildFilter<Person> { Person::name.isNotNull } .toString() }
            expect("name IS NULL") { buildFilter<Person> { Person::name.isNull } .toString() }
        }

        test("composed filters") {
            expect("(name < 'John' and age >= '5')") { buildFilter<Person> { (Person::name lt "John") and (Person::age ge 5) } .toString() }
            expect("(name < 'John' or age >= '5')") { buildFilter<Person> { (Person::name lt "John") or (Person::age ge 5) } .toString() }
        }
    }

    test("and") {
        val and = buildFilter<Person> { (Person::name lt "Name") and (Person::age ge 5) }
        expect(false) { and.test(Person("name 5", 2)) }
        expect(false) { and.test(Person("name 5", 10)) }
        expect(false) { and.test(Person("Na", 2)) }
        expect(true) { and.test(Person("Na", 5)) }
        expect(true) { and == buildFilter<Person> { (Person::name lt "Name") and (Person::age ge 5) } }
        expect(false) { and == buildFilter<Person> { (Person::name lt "Name") and (Person::age ge 10) } }
        expect(false) { and == buildFilter<Person> { (Person::name lt "Name a") and (Person::age ge 5) } }
    }

    test("or") {
        val or = buildFilter<Person> { (Person::name lt "Name") or (Person::age ge 5) }
        expect(false) { or.test(Person("name 5", 2)) }
        expect(true) { or.test(Person("name 5", 10)) }
        expect(true) { or.test(Person("Na", 2)) }
        expect(true) { or.test(Person("Na", 5)) }
        expect(true) { or == buildFilter<Person> { (Person::name lt "Name") or (Person::age ge 5) } }
        expect(false) { or == buildFilter<Person> { (Person::name lt "Name") and (Person::age ge 5) } }
        expect(false) { or == buildFilter<Person> { (Person::name lt "Name") or (Person::age ge 10) } }
        expect(false) { or == buildFilter<Person> { (Person::name lt "Name a") or (Person::age ge 5) } }
    }
})
