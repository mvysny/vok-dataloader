package com.github.mvysny.vokdataloader

import com.github.mvysny.dynatest.DynaTest
import com.github.mvysny.dynatest.expectList
import kotlin.test.expect

class FilteredDataLoaderTest : DynaTest({
    test("simple") {
        val dl: DataLoader<Person> = (0..10).map { Person("name $it", it) }.dataLoader().withFilter { Person::age ge 5 }
        expect((5..10).toList()) { dl.fetch().map { it.age } }
        expect(6) { dl.getCount() }
    }
    test("combining filters") {
        val dl: DataLoader<Person> = (0..10).map { Person("name $it", it) }.dataLoader().withFilter { Person::age ge 5 }
        expect((5..7).toList()) { dl.fetch(filter = buildFilter { Person::age le 7 }).map { it.age } }
        expect(3) { dl.getCount(filter = buildFilter { Person::age le 7 }) }
    }
})
